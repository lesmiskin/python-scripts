#!/usr/bin/python

# This script prints a pair of DNA random DNA sequences, the second 
# of which is the reverse-complement of the first.

import random, Bio
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna, IUPAC

DNA_LETTERS = IUPAC.IUPACUnambiguousDNA().letters

def writePair(length):
	sequence = ""
	for x in range(length): sequence += random.choice(DNA_LETTERS)
	dna = Seq(sequence, generic_dna)
	print sequence
	print dna.complement()

#-----------------------------------------------------------------

TEST_PAIR_REPEAT = 3
TEST_SEQUENCE_LENGTH = 200

for _ in range(TEST_PAIR_REPEAT):
	writePair(TEST_SEQUENCE_LENGTH)
